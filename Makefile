LATEXMK=latexmk
LATEXMK_OPT=-xelatex -synctex=1

MAIN_BUILD_FILE=main
CLEAN_FILES=*.aux *.fdb_latexmk *.fls *.log *.out *.blg *.bbl *.ps *.synctex.gz *.toc *.nav *.snm *.xdv

all: clear build clean

clean:
	rm -f $(CLEAN_FILES)

clear: clean
	rm -f $(MAIN_BUILD_FILE).pdf

build: *.tex
	$(LATEXMK) $(LATEXMK_OPT) $(MAIN_BUILD_FILE).tex

docker:
	docker build -t docker-latex .
	docker run --rm -ti -v ${PWD}:/build:Z docker-latex bash -c "make clean clear && make build"